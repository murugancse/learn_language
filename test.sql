-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 06:53 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `country_id`, `state_id`, `city_name`, `updated_at`, `created_at`, `status`) VALUES
(1, 130, 1, 'Sibu', '2019-07-11 16:30:41', NULL, 1),
(2, 130, 15, 'Shah Alam', '2019-07-11 16:30:41', NULL, 1),
(3, 130, 1, 'Bintulu', '2019-07-11 16:30:41', NULL, 1),
(4, 130, 1, 'Kuching', '2019-07-11 16:30:41', NULL, 1),
(5, 130, 2, 'Victoria', '2019-07-11 16:30:41', NULL, 1),
(6, 130, 3, 'Taiping', '2019-07-11 16:30:41', NULL, 1),
(7, 130, 4, 'Chukai', '2019-07-11 16:30:41', NULL, 1),
(8, 130, 5, 'Kuala Lumpur', '2019-07-11 16:30:41', NULL, 1),
(9, 130, 6, 'Kangar', '2019-07-11 16:30:41', NULL, 1),
(10, 130, 7, 'Alor Setar', '2019-07-11 16:30:41', NULL, 1),
(11, 130, 8, 'George Town', '2019-07-11 16:30:41', NULL, 1),
(12, 130, 8, 'Butterworth', '2019-07-11 16:30:41', NULL, 1),
(13, 130, 7, 'Sungai Petani', '2019-07-11 16:30:41', NULL, 1),
(14, 130, 9, 'Sandakan', '2019-07-11 16:30:41', NULL, 1),
(15, 130, 9, 'Lahad Datu', '2019-07-11 16:30:42', NULL, 1),
(16, 130, 10, 'Kota Baharu', '2019-07-11 16:30:42', NULL, 1),
(17, 130, 11, 'Johor Bahru', '2019-07-11 16:30:42', NULL, 1),
(18, 130, 11, 'Keluang', '2019-07-11 16:30:42', NULL, 1),
(19, 130, 4, 'Kuala Terengganu', '2019-07-11 16:30:42', NULL, 1),
(20, 130, 3, 'Ipoh', '2019-07-11 16:30:42', NULL, 1),
(21, 130, 12, 'Kuantan', '2019-07-11 16:30:42', NULL, 1),
(22, 130, 9, 'Kota Kinabalu', '2019-07-11 16:30:42', NULL, 1),
(23, 130, 9, 'Tawau', '2019-07-11 16:30:42', NULL, 1),
(24, 130, 12, 'Kuala Lipis', '2019-07-11 16:30:42', NULL, 1),
(25, 130, 3, 'Teluk Intan', '2019-07-11 16:30:42', NULL, 1),
(26, 130, 13, 'Malacca', '2019-07-11 16:30:42', NULL, 1),
(27, 130, 11, 'Muar', '2019-07-11 16:30:42', NULL, 1),
(28, 130, 1, 'Miri', '2019-07-11 16:30:43', NULL, 1),
(29, 130, 14, 'Seremban', '2019-07-11 16:30:43', NULL, 1),
(30, 130, 14, 'Putrajaya', '2019-07-11 16:30:43', NULL, 1),
(31, 130, 12, 'Raub', '2019-07-11 16:30:43', NULL, 1),
(32, 130, 11, 'Batu Pahat', '2019-07-11 16:30:43', NULL, 1),
(33, 130, 15, 'Kelang', '2019-07-11 16:30:43', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`, `updated_at`, `created_at`, `status`) VALUES
(130, 'MY', 'Malaysia', '2019-07-11 15:46:19', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `is_married` int(11) NOT NULL,
  `image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `name`, `age`, `gender`, `is_married`, `image`) VALUES
(1, 'Murugan', 23, 1, 1, ''),
(2, 'hh', 21, 1, 1, ''),
(3, 'new', 22, 0, 0, ''),
(4, 'news', 22, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `country_id`, `state_name`, `updated_at`, `created_at`, `status`) VALUES
(1, 130, 'Sarawak', '2019-07-11 16:03:26', NULL, 1),
(2, 130, 'Labuan', '2019-07-11 16:03:26', NULL, 1),
(3, 130, 'Perak', '2019-07-11 16:03:26', NULL, 1),
(4, 130, 'Terengganu', '2019-07-11 16:03:26', NULL, 1),
(5, 130, 'Kuala Lumpur ', '2019-07-11 16:03:26', NULL, 1),
(6, 130, 'Perlis', '2019-07-11 16:03:26', NULL, 1),
(7, 130, 'Kedah', '2019-07-11 16:03:26', NULL, 1),
(8, 130, 'Pulau Pinang', '2019-07-11 16:03:26', NULL, 1),
(9, 130, 'Sabah', '2019-07-11 16:10:55', NULL, 1),
(10, 130, 'Kelantan', '2019-07-11 16:11:02', NULL, 1),
(11, 130, 'Johor', '2019-07-11 16:11:06', NULL, 1),
(12, 130, 'Pahang', '2019-07-11 16:11:09', NULL, 1),
(13, 130, 'Melaka', '2019-07-11 16:11:13', NULL, 1),
(14, 130, 'Negeri Sembilan', '2019-07-11 16:11:19', NULL, 1),
(15, 130, 'Selangor', '2019-07-11 16:11:22', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
