<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	
	//links labels
	'login_text' => 'Sign in to start your session',
	'welcome_message'	 =>	'Welcome',
	'welcome_message_to'=>'To Admin Panel',
	'login'=>'Login',
	'login_page_name'=>'Login',
	
	
	//login page
	'login_text' => 'Sign in to start your session',
	'welcome_message'	 =>	'Welcome',
	'welcome_message_to'=>'To Admin Panel',
	'login'=>'Login',
	'login_page_name'=>'Login',
	
	//header 
    'admin' => 'Admin',
	'profile_link' => 'Profile',
	'sign_out' => 'Sign out',
	'toggle_navigation'=>'Toggle navigation',
	'you_have'=>'You have',	
	'new_users'=>'new user(s).',
	
	
];
